package com.OsSeeker;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

/**
 * Created by iota on 09/05/2015.
 */
public class Utility {
    public static BufferedReader getInstallLogReader(Path filePath, Timestamp fromWhen) {
        BufferedReader bfReader = null;
        try {
            bfReader = Files.newBufferedReader(filePath, Charset.forName("US-ASCII"));
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return bfReader;
    }

    public static Timestamp getTimestampFromLogTimeStamp(String LogTimeStamp){
        return new Timestamp(Long.parseLong(getTimestampStringInMillis(LogTimeStamp)));
    }

    public static String getTimestampStringInMillis(String LogTimeStamp) {
        //the input timestamp is in seconds, e.g. "1430825689.00" or "1430825689.0"
        //(unlikely to be "1430825689" but the code also handles this case)
        //java.sql.Timestamp needs timestamp in milliseconds, e.g "1430825689000"
        String[] newSubStrings = LogTimeStamp.split("[.]");
        String newString = newSubStrings[0];
        if(newSubStrings.length==2){
            if(newSubStrings[1].length()==1){newSubStrings[1]+="00";}
            if(newSubStrings[1].length()==2){newSubStrings[1]+="0";}
            newString += newSubStrings[1];
        }else if(newSubStrings.length==1){
            newString += "000";
        }
        return newString;
    }

    public static String formatMilliseconds(long millis){
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static class HostName {
        private String ipv4;
        private HostName(String ipv4){
            this.ipv4=ipv4;
        }
        public static HostName getInstance(String ip){
            return new HostName(ip);
        }
        @Override
        public String toString(){
            return ipv4;
        }
    }

    public static class QuickSort {
        public static void sortDefaultAscending(Timestamp[] sortee){
            sort(sortee, 0, sortee.length-1, true);
        }

        private static void sort(Timestamp[] sortee, int leftmostIdx, int rightmostIdx, boolean ascending){

            if(leftmostIdx < rightmostIdx){
                int pivotIdx = partition(sortee, leftmostIdx, rightmostIdx);
                sort(sortee, leftmostIdx, pivotIdx - 1, ascending);
                sort(sortee, pivotIdx + 1, rightmostIdx, ascending);
            }
        }

        private static int partition(Timestamp[] sortee, int leftmostIdx, int rightmostIdx){
            int pivotIdx = (rightmostIdx+leftmostIdx)/2;
            Timestamp pivotVal = sortee[pivotIdx];
            swap(sortee, pivotIdx, rightmostIdx);
            int cursorIdx = leftmostIdx;
            for(int i=leftmostIdx;i<rightmostIdx;i++){
                if(sortee[i].getTime()<=pivotVal.getTime()){
                    swap(sortee, cursorIdx, i);
                    cursorIdx+=1;
                }
            }
            swap(sortee, cursorIdx, rightmostIdx);
            return cursorIdx;
        }

        private static void swap(Timestamp[] sortee, int idx_a, int idx_b){
            Timestamp swap_slot = sortee[idx_a];
            sortee[idx_a] = sortee[idx_b];
            sortee[idx_b] = swap_slot;
        }
    }
}
