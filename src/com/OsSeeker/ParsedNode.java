package com.OsSeeker;

import com.jcraft.jsch.JSch;

import java.sql.Timestamp;
import java.util.Comparator;

/**
 * Created by iota on 09/05/2015.
 */
public class ParsedNode {
    private String host;
    private String profileType;
    private String osProfile;
    private Timestamp timeStampStart;
    private Timestamp timeStampStop;
    private int status;
    public static final int STATUS_UNKNOWN = 0;
    public static final int STATUS_PENDING = 2;
    public static final int STATUS_STOPPED = 4;
    public static final int STATUS_HEADLESS = 8;

    private ParsedNode(String hostName, String profileType, String osProfile, Timestamp timeStampStart, Timestamp timeStampStop){
        this.host = hostName;
        this.profileType =profileType;
        this.osProfile =osProfile;
        this.timeStampStart=timeStampStart;
        this.timeStampStop=timeStampStop;
        long duration = timeStampStop.getTime()-timeStampStart.getTime();
        if(duration>=0){
            status = STATUS_STOPPED;
        }
        else{
            status = STATUS_PENDING;
        }
    }

    public ParsedNode setStatusHeadless(){
        status = STATUS_HEADLESS;
        return this;
    }


    public Timestamp getTimeStampStart(){
        return timeStampStart;
    }

    public Timestamp getTimeStampStop(){
        return timeStampStop;
    }

    public int getStatus(){
        return status;
    }

    public String getStatusMessage(){
        String msg = "";
        switch (getStatus()){
            case STATUS_UNKNOWN:
                msg = "! Unknown status";
                break;
            case STATUS_PENDING:
                msg = "Pending or installation exited abnormally, STARTED on "+timeStampStart.toString();
                break;
            case STATUS_STOPPED:
                msg = "Stopped on "+timeStampStop.toString()+", duration: "
                        +Utility.formatMilliseconds(timeStampStop.getTime()-timeStampStart.getTime());
                break;
            case STATUS_HEADLESS:
                msg = "No start timestamp in record, but STOPPED on "+timeStampStop.toString();
            default:
                break;
        }
        return msg;
    }

    static ParsedNode getInstance(String hostName, String profileType, String osName, Timestamp timeStampStart, Timestamp timeStampStop){
        return new ParsedNode(hostName, profileType, osName, timeStampStart, timeStampStop);
    }

    public static class TimeGeneralComparator implements Comparator<ParsedNode> {
        @Override
        public int compare(ParsedNode p1, ParsedNode p2) {
            /*
            if(p1.getStatus()==p2.getStatus()){
                if(p1.getStatus()==ParsedNode.STATUS_STOPPED){
                    return p1.getTimeStampStop().compareTo(p2.getTimeStampStop());
                }else if()
            }
            */
            return p1.getTimeStampStart().compareTo(p2.getTimeStampStart());
        }
    }

    public static class TimeStartedComparator implements Comparator<ParsedNode> {
        @Override
        public int compare(ParsedNode p1, ParsedNode p2) {
            return p1.getTimeStampStart().compareTo(p2.getTimeStampStart());
        }
    }

    public static class TimeStoppedComparator implements Comparator<ParsedNode> {
        @Override
        public int compare(ParsedNode p1, ParsedNode p2) {
            return p1.getTimeStampStop().compareTo(p2.getTimeStampStop());
        }
    }

    @Override
    public String toString(){
        //return host +" "+ profileType +" "+ osProfile +" "+timeStampStart+" "+timeStampStop;
        return host +"\t"+ profileType +"\t"+ osProfile +"\t"+getStatusMessage();
    }
}
