package com.OsSeeker;

import com.jcraft.jsch.*;
import org.apache.commons.io.input.CloseShieldInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;


/**
 * Created by chenhui on 04/04/2015.
 * This class can store all information for one node and can hold all
 * possible actions on one node. There can be three class inherited
 * from the Node class: HeadNode, PxeNode and TestNode.
 */
public class Node {
    //this variable is used for debugging. 
    //Sometimes, you need to force it to reset the user prefs
    static boolean FORCE_UPDATE_PREFS = false;

    final static String DEFAULT_USER = "root";
    final static String DEFAULT_PASSWORD = "Boston2015";
    //this is default user name and password for Node 
    final static String INITIALIZED_KEY = "Initialized";
    final static String USER_KEY = "User";
    final static String PSWD_KEY = "Password";
    final static String HOST_KEY = "Host";

    String host;

    String user;
    String pswd;
    int port = 22;

    JSch ssher;
    MyUserInfo ui = new MyUserInfo();

    //to store the preference
    Preferences prefs;


    //this should be the main method for creating Node objects
    //it will try to find the preference, 
    //  if exited, it will simply read in and do the initialization
    //  if not, it should ask for them and do the initialization
    public Node( JSch j) {
        ssher = j;
        initPrefs();
    }

    public Node(String host, String user, String pswd, JSch j) {
        this.host = host;
        this.user = user;
        this.pswd = pswd;
        this.ssher = j;
    }

    public Node(String host, String user, String pswd, JSch j, int p) {
        this.host = host;
        this.user = user;
        this.pswd = pswd;
        this.ssher = j;
        this.port = p;
    }

    void initPrefs(){
        prefs = Preferences.userRoot().node(this.getClass().getName());
    }

    void initNode(){
        if (prefInitialized() && (!FORCE_UPDATE_PREFS) ) {
            System.out.println("Preferences is already initialized for " + this.getClass().getName()+ "\n" + prefs);
            this.host = prefs.get(HOST_KEY, null);
            this.user = prefs.get(USER_KEY, DEFAULT_USER);
            this.pswd = prefs.get(PSWD_KEY, DEFAULT_PASSWORD);
        } else {
            askInput();
            updatePrefs();
        }
    }

    //the method will ask user for information and change them.
    void askInput(){
        //Scanner reader = new Scanner(System.in);
        Scanner reader = new Scanner(new CloseShieldInputStream(System.in));
        System.out.println("Please input host for " + this.getClass().getName());
        host = reader.nextLine();
        System.out.println("Please input user for " + this.getClass().getName());
        this.user = reader.nextLine();
        System.out.println("Please input password for " + this.getClass().getName());
        this.pswd = reader.nextLine();
        //when closing this reader, the System.in will also be closed which affects the other 
        //nodes for asking input. For now, just leave it open.
        reader.close();
    }

    void updatePrefs(){
        prefs.put(HOST_KEY, this.host);
        prefs.put(USER_KEY, this.user);
        prefs.put(PSWD_KEY, this.pswd);
        prefs.put(INITIALIZED_KEY, "True");
    }

    //this method is used for clearing the preferences
    void resetPrefs(){
        System.out.println("Reseting the Preferences for " + this.getClass().getName());
        try {
            prefs.clear();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }
        askInput();
        updatePrefs();
    }

    void updatePassword(String password) {
        if ( !this.pswd.equals(password) && password != null ) {
            System.out.println("Updating the password...");
            this.pswd = password;
            prefs.put(PSWD_KEY, password);
        }
    }


    private boolean prefInitialized(){
       return (prefs.get(INITIALIZED_KEY, null) != null);
    }

    protected Session getConnectedSession(String user, String host, int port){
        Session s = null;
        try {
            s = ssher.getSession(user, host, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        s.setPassword(pswd);
        s.setUserInfo(ui);
        try {
            s.connect();
        } catch (JSchException e) {
            //e.printStackTrace();
            System.out.println("Connected cancelled by user!");
            System.exit(1);
        }
        updatePassword(ui.password);
        return s;
    }
    //check if the connect is ready and get output from ls command
    public void selfCheck(){
        String command = "ls ./";
        try{
            Session session = this.getConnectedSession(this.user, this.host, this.port);
            //Test if the session is connected.
            boolean isSessionConnected = session.isConnected();
            System.out.println("============================System Self Check======================");
            System.out.println(host + " System Connected:" + isSessionConnected);

            // open a channel
            Channel channel = session.openChannel("exec");
            //set execution command
            ((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec)channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();

            channel.connect();

            System.out.println("Output of the command:" + command );
            byte[] tmp=new byte[1024];
            while(true){
                while(in.available()>0){
                    int i=in.read(tmp, 0, 1024);
                    if(i<0)break;
                    System.out.print(new String(tmp, 0, i));
                }
                if(channel.isClosed()){
                    if(in.available()>0) continue; 
                    System.out.println("exit-status: "+channel.getExitStatus());
                    break;
                }
                try{Thread.sleep(1000);}catch(Exception ee){}
            }
            System.out.println("============================END of System Self Check======================");
            channel.disconnect();
            session.disconnect();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    //this method is used for testing if the node is reachable
    public boolean isReachable() {
        boolean reachable = false;
        InetAddress inet = null;
        try {
            inet = InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            reachable = inet.isReachable(5000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return reachable;
    }
}

