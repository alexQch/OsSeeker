package com.Test;


import com.jcraft.jsch.*;
import java.io.*;//imported java io

// This class is mainly used for SSH actions
// Ideally, one instance of Ssher class should corresponds to
// one instance of node so that they can do separate actions at the same time( maybe in parallel )
//
public class Ssher {
    // claim the local vars
    String user;
    String host;
    String password;

    String command = null;


    //Constructor: for one Ssher instance to functioning.
    //we need at least user name, password and host be ready.
    public Ssher(String host, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;
    }

    //getter and setter for var command
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    void testConnect() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            session.setPassword(password);

            //MyUserInfo realized the passwordless connection.
            UserInfo ui = new TestUserInfo();
            session.setUserInfo(ui);
            session.connect();
            //Test if the session is connected.
            boolean isSessionConnected = session.isConnected();
            System.out.println("System Status:" + isSessionConnected);
            session.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //This method is simply added for testing connection to the server
	void testLs(){
        try{
            JSch jsch=new JSch();
            Session session=jsch.getSession(user, host, 22);
            session.setPassword(password);

            //MyUserInfo realized the passwordless connection.
            UserInfo ui=new TestUserInfo();
            session.setUserInfo(ui);
            session.connect();
            //Test if the session is connected.
            boolean isSessionConnected = session.isConnected();
            System.out.println("System Status:" + isSessionConnected);

            // open a channel
            Channel channel = session.openChannel("exec");
            //set execution command
            ((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec)channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();

            channel.connect();

            System.out.println("Output of the command:");
            byte[] tmp=new byte[1024];
            while(true){
                while(in.available()>0){
                    int i=in.read(tmp, 0, 1024);
                    if(i<0)break;
                    System.out.print(new String(tmp, 0, i));
                }
                if(channel.isClosed()){
                    if(in.available()>0) continue; 
                    System.out.println("exit-status: "+channel.getExitStatus());
                    break;
                }
                try{Thread.sleep(1000);}catch(Exception ee){}
            }
            channel.disconnect();

            session.disconnect();
        }catch(Exception e){
            System.out.println(e);
        }
    }

    // Test the method of get files from the remote server
    void testSftp(){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            session.setPassword(password);

            //MyUserInfo realized the passwordless connection.
            UserInfo ui = new TestUserInfo();
            session.setUserInfo(ui);
            session.connect();
            //Test if the session is connected.
            boolean isSessionConnected = session.isConnected();
            System.out.println("System Status:" + isSessionConnected);

            //started the sftp channel
            Channel channel=session.openChannel("sftp");
            channel.connect(); //connect the channel
            ChannelSftp c=(ChannelSftp)channel;
            
            //get the file
            c.get("Test", "testFromRemote");

            c.quit(); // quit the channel
            session.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // Test the jump host function. This can be really useful when connecting from
    // the outside network where only headnode is reachable. The work mode is 
    // 1. connect to head node
    // 2. connect to pxe node 
    // 3. get the file wanted
    public void testJumpHost(){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            session.setPassword(password);

            //MyUserInfo realized the passwordless connection.
            UserInfo ui = new TestUserInfo();
            session.setUserInfo(ui);
            session.connect();
            //Test if the session is connected.
            boolean isSessionConnected = session.isConnected();
            System.out.println("System Status:" + isSessionConnected);

            //create new users for pxe node
            String pxeHost = "10.17.1.1";
            String pxeUser = "chenhui";
            String pxePswd = "Boston2014!1";

            //forward the local port to 22 port on pxe node
            int assignPort = session.setPortForwardingL(0, pxeHost, 22);
            System.out.println("The assigned port is " + assignPort );
            //create another session for pxe node

            Session pxeSession = jsch.getSession(pxeUser, "127.0.0.1", assignPort);
            pxeSession.setPassword(pxePswd);
            pxeSession.setUserInfo(ui);
            pxeSession.connect();
            isSessionConnected = pxeSession.isConnected();
            System.out.println("System Status:" + pxeHost + ":" + isSessionConnected);

            //start sftp channel
            ChannelSftp sftp = (ChannelSftp)pxeSession.openChannel("sftp");

            sftp.connect();

            sftp.ls(".",
                    new ChannelSftp.LsEntrySelector() {
                        public int select(ChannelSftp.LsEntry le) {
                            System.out.println(le);
                            return ChannelSftp.LsEntrySelector.CONTINUE;
                        }
                    });

            sftp.disconnect();


            //disconnect all sessions
            pxeSession.disconnect();
            session.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
