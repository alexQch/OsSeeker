package com.Test;

import com.OsSeeker.*;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.SftpException;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by chenhui on 11/04/2015.
 */
public class Tester {
    public static void main(String[] args) throws SftpException {
        String user = "temp";
        String host = "head.bostonhpc.co.uk";
        String password = "Boston2014";
		Ssher ssh = new Ssher(host, user, password);
        ssh.setCommand("ls ./");
		//ssh.testSftp();
        System.out.println("This is the tester class ");
        
        //look for files in classpath
        Tester obj = new Tester();
        //System.out.println(obj.getFile("./resource/install.log"));
        obj.testParser();
        //obj.testUtility();
        //obj.testMultiThreadSsher(ssh);
        //obj.testPreferences();
        //obj.testNode();
        //obj.testSP();
        //obj.testSsher();
        //Tester.fileExists("install.log");


    }

    //this method is designed to check if file existed in classpath
    public static boolean fileExists(String fileName){
        URL filePath = Tester.class.getClassLoader().getResource(fileName);
        if(filePath != null) {
            System.out.println(fileName + " found");
            return true;
        }
        System.out.println(fileName + " not found");
        return false;
    }
    //test simplePresenter
    void testSP(){
        String fileName = "install.log";
        SimplePresenter.printOsInfo(new File(fileName));
    }

    //test the Node class
    void testNode(){
        JSch j = new JSch();
        Node n = new Node(j);
        System.out.println("System is reachable?" + n.isReachable());
        n.selfCheck();

        HeadNode headNode = new HeadNode(j);
        headNode.selfCheck();

        PxeNode pxeNode = new PxeNode(j);
        pxeNode.selfCheck();
    }

    //testing the preference
    public void testPreferences(){
        Preferences prefs = Preferences.userRoot().node("com/Test");
        int numRows = prefs.getInt("Rows", 40);
        String color = prefs.get("Color", "Blue");
        prefs.putInt("Rows", 631216);
        prefs.put("Color", "GRAY");
        System.out.println(prefs.getInt("Rows", 40));
        System.out.println(prefs.get("Color", "Blue"));
        try {
            prefs.clear();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }
        System.out.println(prefs.getInt("Rows", 0));
        System.out.println(prefs.get("Color", "Black"));

    }

    //method for testing IpParser
    public void testParser(){
        //Test parser
        long time0 = System.currentTimeMillis();
        Path input = Paths.get("D://install.log");
        ArrayList<ParsedNode> output = IpParser.parse(input, new Timestamp(new Long("14066241460470")));
        for(ParsedNode pnode : output){
            pnode.toString();
            System.out.println(pnode.toString());
        }
        long time1 = System.currentTimeMillis();
        System.out.println("time used: "+(time1-time0)+" ms");
    }

    public void testUtility(){
        System.out.println("expected:\t2015-05-05 19:34:49.123\nout:\t"+Utility.getTimestampFromLogTimeStamp("1430825689"+".123"));
        System.out.println("expected:\t2015-05-05 19:34:49.12\nout:\t"+Utility.getTimestampFromLogTimeStamp("1430825689"+".12"));
        System.out.println("expected:\t2015-05-05 19:34:49.1\nout:\t"+Utility.getTimestampFromLogTimeStamp("1430825689"+".1"));
        System.out.println("expected:\t2015-05-05 19:34:49.0\nout:\t"+Utility.getTimestampFromLogTimeStamp("1430825689"+""));
    }


    public void testSsher(){
        Ssher ssh = new Ssher("head.bostonhpc.co.uk","temp", "Boston2014-");
		ssh.testConnect(); // Test the jump host method
    }

    private String getFile(String fileName) {

        StringBuilder result = new StringBuilder("");

        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();

    }
}
